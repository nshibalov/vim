set nocompatible
set encoding=utf-8
set fileencodings=usc-bom,utf-8,utf-16le,cp1251
set termencoding=utf-8

if has('win32')
    let $VIMHOME = expand("~/vimfiles")
  else
    let $VIMHOME = expand("~/.vim")
endif

" Vundle

filetype off
set rtp+=$VIMHOME/bundle/Vundle.vim
call vundle#begin('$VIMHOME/bundle')

Plugin 'VundleVim/Vundle.vim'
Plugin 'w0rp/ale.git'
Plugin 'Valloric/YouCompleteMe.git'
Plugin 'vim-airline/vim-airline.git'
Plugin 'vim-airline/vim-airline-themes.git'
Plugin 'scrooloose/nerdtree.git'
Plugin 'chriskempson/base16-vim.git'
Plugin 'pangloss/vim-javascript.git'
Plugin 'tpope/vim-fugitive.git'
Plugin 'mxw/vim-jsx.git'
Plugin 'sebdah/vim-delve'

call vundle#end()

" Settings
syntax on
filetype plugin indent on

set backupdir=$VIMHOME/dir_temp,.
set directory=$VIMHOME/dir_swap,.
set undodir=$VIMHOME/dir_undo,.
set undofile

set autoindent
set backspace=indent,eol,start
set colorcolumn=80
set expandtab
set foldmethod=syntax
set history=50 " keep 50 lines of command line history
set hlsearch
set incsearch  " do incremental searching
set laststatus=2
set listchars=eol:$,tab:>-,trail:~,extends:>,precedes:<
set list
set mouse=a
set number
set ruler      " show the cursor position all the time
set shiftround
set shiftwidth=2
set showcmd    " display incomplete commands
set showmatch
set smartindent
set softtabstop=2
set tabstop=2

" GUI
set guioptions-=m "remove menu bar
set guioptions-=T "remove toolbar
set guioptions-=r "remove right-hand scroll bar
set guioptions-=L "remove left-hand scroll barset term=xterm
"
if has('win32')
  set guifont=Hack:h12
  set rop=type:directx,level:0.75,gamma:1.25,contrast:0.25,geom:1,renmode:5,taamode:1
else
  set guifont=Hack\ 12
endif

if has("gui_running")
  colorscheme base16-twilight
else
  colorscheme twilight256c
endif

" ALE
let g:ale_open_list=1
let g:ale_lint_on_text_changed='normal'
let g:ale_lint_on_insert_leave=1
let g:ale_echo_msg_error_str='E'
let g:ale_echo_msg_warning_str='W'
let g:ale_echo_msg_format="[%severity%] [%linter%] [%code%] %s"
let g:ale_linters={
\   'javascript': ['eslint'],
\   'python': ['python', 'pylint'],
\   'cpp': ['clang', 'clangtidy', 'clangcheck'],
\}

let g:ale_python_pylint_options="
\ --disable=missing-docstring
\ --max-line-length=79
\ --extension-pkg-whitelist=pyodbc,PyQt4,PySide,PySide2,cefpython3
\"

let s:checkers_dir=resolve(expand("$VIMHOME/checkers"))

let g:ale_cpp_clang_executable=s:checkers_dir."/clang.py"
let g:ale_cpp_clangtidy_executable=s:checkers_dir."/clang_tidy.py"
let g:ale_cpp_clangcheck_executable=s:checkers_dir."/clang_check.py"
let g:ale_go_staticcheck_lint_package=1

" AirLine
let g:airline_theme="base16_twilight"
let g:airline_powerline_fonts=1

" NerdTree
let g:NERDTreeDirArrowExpandable="+"
let g:NERDTreeDirArrowCollapsible="~"
let g:NERDTreeWinPos="right"
let g:NERDTreeIgnore=['\.pyc$']

" YCM
let g:ycm_register_as_syntastic_checker=0
let g:ycm_confirm_extra_conf=0
let g:ycm_python_binary_path='python'
nnoremap f :YcmCompleter GoToDefinition<CR>
nnoremap F :tab split \| YcmCompleter GoToDefinition<CR>

" JSX
let g:jsx_ext_required=0 " Allow JSX in normal JS files

" Mapping
map <C-n> :NERDTreeToggle<CR>
nnoremap <silent> <F5> :silent !"%" & pause<CR>
