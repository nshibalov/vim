# coding=utf-8

import os
import sys
from subprocess import call


FLAGS_VAR = "FLAGS"
CONFIG_NAME = ".project.py"


def _load_flags(_fname):
    wdir = os.path.dirname(_fname)
    while True:
        cfg_fname = os.path.join(wdir, CONFIG_NAME)
        if os.path.exists(cfg_fname):
            locals_dict = {}
            exec(open(cfg_fname, "rb").read(), {}, locals_dict)
            if FLAGS_VAR in locals_dict:
                return locals_dict[FLAGS_VAR]
            return []

        ndir = os.path.dirname(wdir)
        if ndir == wdir:
            break
        wdir = ndir
    return []


def cmd_gen(_checker_exe):
    cmd = [_checker_exe]
    cmd += sys.argv[1:]

    if _checker_exe in ["clang-check", "clang-tidy"]:
        cmd += ["--"]

    print(cmd)
    cmd += _load_flags(sys.argv[-1])
    print(cmd)

    call(cmd)
